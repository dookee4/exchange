﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exchange
{
    class HistoryTable
    {
        public HistoryTable(int Id, string FromAmount, string FromCurrency, string ToAmount, string ToCurrency, DateTime DateWhen)
        {
            this.Id = Id;
            this.FromAmount = FromAmount;
            this.FromCurrency = FromCurrency;
            this.ToAmount = ToAmount;
            this.ToCurrency = ToCurrency;
            this.DateWhen = DateWhen;
        }
        public int Id { get; set; }
        public string FromAmount { get; set; }
        public string FromCurrency { get; set; }
        public string ToAmount { get; set; }
        public string ToCurrency { get; set; }
        public DateTime DateWhen { get; set; }
    }
}
