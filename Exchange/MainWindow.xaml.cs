﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Exchange
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        ApiService apiService = new ApiService();
        List<HistoryTable> result = new List<HistoryTable>();
        private async void ExchangeBTN_Click(object sender, RoutedEventArgs e)
        {
            var ratio = await GetRatioInfo();
            ConvertToTB.Text = Currency_Calculator(ratio).ToString("F5");
            AddToHistory();
        }

        private decimal Currency_Calculator(decimal ratio)
        {
            decimal valueFrom = Convert.ToDecimal(ConvertFromTB.Text);
            decimal newValue;
            newValue = valueFrom * ratio;
            return newValue;
        }


        private async void Reverse(object sender, MouseButtonEventArgs e)
        {
            var temp_value = ConvertFromCurrCB.Text;
            ConvertFromCurrCB.Text = ConvertToCurrCB.Text;
            ConvertToCurrCB.Text = temp_value;

            var ratio = await apiService.GetRatio(ConvertFromCurrCB.Text, ConvertToCurrCB.Text);

            ConvertFromTB.Text = ConvertToTB.Text;
            ConvertToTB.Text = Currency_Calculator(ratio).ToString("F5");
            RatioLB.Content = "1 " + ConvertFromCurrCB.Text + " = " + ratio.ToString("F5") + " " + ConvertToCurrCB.Text;
        }

        private void AddToHistory()
        {
            result.Add(new HistoryTable(result.Count + 1, ConvertFromTB.Text, ConvertFromCurrCB.Text, ConvertToTB.Text, ConvertToCurrCB.Text, DateTime.Now));
            History_DataGrid.ItemsSource = result.ToList();
        }

        private async Task<decimal> GetRatioInfo()
        {
            string currencyFromStr = ConvertFromCurrCB.Text;
            string currencyToStr = ConvertToCurrCB.Text;
            var ratio = await apiService.GetRatio(currencyFromStr, currencyToStr);
            RatioLB.Content = "1 " + currencyFromStr + " = " + ratio.ToString("F5") + " " + currencyToStr;
            return ratio;
        }

        private async void ConvertFrom_DropClosed(object sender, EventArgs e)
        {
            if (ConvertFromCurrCB.SelectedIndex == ConvertToCurrCB.SelectedIndex)
            {
                if (ConvertFromCurrCB.SelectedIndex < ConvertFromCurrCB.Items.Count - 1)
                    ConvertFromCurrCB.SelectedIndex++;
                else ConvertFromCurrCB.SelectedIndex--;
            }
            await GetRatioInfo();
        }

        private async void ConvertTo_DropClosed(object sender, EventArgs e)
        {
            if (ConvertToCurrCB.SelectedIndex == ConvertFromCurrCB.SelectedIndex)
            {
                if (ConvertToCurrCB.SelectedIndex < ConvertToCurrCB.Items.Count - 1)
                    ConvertToCurrCB.SelectedIndex++;
                else ConvertToCurrCB.SelectedIndex--;
            }
            await GetRatioInfo();
        }

        private void HistoryBTN_Click(object sender, RoutedEventArgs e)
        {
            #region HistoryVisibility

            History_DataGrid.Visibility = Visibility.Visible;
            SearchIDTB.Visibility = Visibility.Visible;
            SearchFromCurrTB.Visibility = Visibility.Visible;
            SearchAmountFromTB.Visibility = Visibility.Visible;
            SearchToCurrTB.Visibility = Visibility.Visible;
            SearchAmountToTB.Visibility = Visibility.Visible;
            SearchLB.Visibility = Visibility.Visible;
            BackBTN.Visibility = Visibility.Visible;
            Refresh.Visibility = Visibility.Visible;
            idLB.Visibility = Visibility.Visible;
            AmountLB.Visibility = Visibility.Visible;
            CurrFromLB.Visibility = Visibility.Visible;
            CurrToLB.Visibility = Visibility.Visible;
            ConvertedLB.Visibility = Visibility.Visible;

            ExchangeBTN.Visibility = Visibility.Hidden;
            ExchangeFromLB.Visibility = Visibility.Hidden;
            ExchangeToLB.Visibility = Visibility.Hidden;
            ConvertFromCurrCB.Visibility = Visibility.Hidden;
            ConvertFromTB.Visibility = Visibility.Hidden;
            ConvertToCurrCB.Visibility = Visibility.Hidden;
            ConvertToTB.Visibility = Visibility.Hidden;
            ChangeDirLB.Visibility = Visibility.Hidden;
            RatioLB.Visibility = Visibility.Hidden;
            HistoryBTN.Visibility = Visibility.Hidden;

            #endregion
        }

        private void BackBTN_Click(object sender, RoutedEventArgs e)
        {
            #region HistoryVisibility

            History_DataGrid.Visibility = Visibility.Hidden;
            SearchIDTB.Visibility = Visibility.Hidden;
            SearchFromCurrTB.Visibility = Visibility.Hidden;
            SearchAmountFromTB.Visibility = Visibility.Hidden;
            SearchToCurrTB.Visibility = Visibility.Hidden;
            SearchAmountToTB.Visibility = Visibility.Hidden;
            SearchLB.Visibility = Visibility.Hidden;
            BackBTN.Visibility = Visibility.Hidden;
            Refresh.Visibility = Visibility.Hidden;
            idLB.Visibility = Visibility.Hidden;
            AmountLB.Visibility = Visibility.Hidden;
            CurrFromLB.Visibility = Visibility.Hidden;
            CurrToLB.Visibility = Visibility.Hidden;
            ConvertedLB.Visibility = Visibility.Hidden;

            ExchangeBTN.Visibility = Visibility.Visible;
            ExchangeFromLB.Visibility = Visibility.Visible;
            ExchangeToLB.Visibility = Visibility.Visible;
            ConvertFromCurrCB.Visibility = Visibility.Visible;
            ConvertFromTB.Visibility = Visibility.Visible;
            ConvertToCurrCB.Visibility = Visibility.Visible;
            ConvertToTB.Visibility = Visibility.Visible;
            ChangeDirLB.Visibility = Visibility.Visible;
            RatioLB.Visibility = Visibility.Visible;
            HistoryBTN.Visibility = Visibility.Visible;

            #endregion
        }

        #region Search
        private void Id_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (SearchIDTB.Text != "")
            {
                var filtered = result.Where(row => row.Id == short.Parse(SearchIDTB.Text));
                History_DataGrid.ItemsSource = filtered;
            }  
        }
        private void FromCurr_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (SearchFromCurrTB.Text != "")
            {
                var filtered = result.Where(row => row.FromCurrency.StartsWith(SearchFromCurrTB.Text));
                History_DataGrid.ItemsSource = filtered;
            }
        }
        private void FromAmount_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (SearchAmountFromTB.Text != "")
            {
                var filtered = result.Where(row => row.FromAmount.StartsWith(SearchAmountFromTB.Text));
                History_DataGrid.ItemsSource = filtered;
            }
        }
        private void ToCurr_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (SearchToCurrTB.Text != "")
            {
                var filtered = result.Where(row => row.ToCurrency.StartsWith(SearchToCurrTB.Text));
                History_DataGrid.ItemsSource = filtered;
            }
        }
        private void ToAmount_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (SearchAmountToTB.Text != "")
            {
                var filtered = result.Where(row => row.ToAmount.StartsWith(SearchAmountToTB.Text));
                History_DataGrid.ItemsSource = filtered;
            }
        }
        #endregion

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            History_DataGrid.ItemsSource = result.ToList();
        }
    }
}
