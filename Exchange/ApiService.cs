﻿using System;
using System.Collections.Generic;
using System.Json;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Exchange
{
    public class ApiService
    {
        public ApiService()
        {

        }
        private static readonly HttpClient client = new HttpClient();
        public async Task<decimal> GetRatio(string CurrencyFrom, string CurrencyTo)
        {
            var response = await client.GetStringAsync($"https://api.exchangeratesapi.io/latest?base={CurrencyFrom}&symbols={CurrencyTo}");
            JsonValue json = JsonValue.Parse(response);
            if (json.ContainsKey("rates"))
            {
                var filter = json["rates"];
                if (filter.ContainsKey(CurrencyTo))
                {
                    var jRatio = filter[CurrencyTo];
                    decimal ratio = (Decimal)jRatio;
                    return ratio;
                }
            }
            return 0;
        }
    }
}
